{-# LANGUAGE DataKinds #-}

module Samplizer.Sim where

import System.Random
import qualified Data.Map as M

import Samplizer.Config

splitEvery :: Int -> [a] -> [[a]]
splitEvery _ [] = []
splitEvery n list = first : splitEvery n rest
  where
    (first,rest) = splitAt n list

lenMatches :: (a -> Bool) -> [a] -> Int
lenMatches f = length . filter f

getRandoms :: Int -> Int -> Int -> Int -> IO [[Int]]
getRandoms e1 e2 rs sims = do
  g <- newStdGen
  return $ splitEvery rs . take total $ randomRs (e1, e2) g
  where
    total = sims * rs

simCount :: (Int -> Bool) -> [[Int]] -> M.Map Int Int
simCount f = go (M.fromList [])
  where
    go :: M.Map Int Int -> [[Int]] -> M.Map Int Int
    go = foldl (\acc x -> M.insertWith (+) (lenMatches f x) 1 acc)

execSims :: Options Complete -> IO (M.Map Int Int)
execSims o = do
  rs <- getRandoms (spaceFloor o) (spaceCeiling o) (rolls o) (repeats o)
  return $ simCount (f (targets o)) rs
  where
    f (x1, x2) x = x >= x1 && x <= x2
