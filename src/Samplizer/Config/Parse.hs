{-# LANGUAGE DataKinds #-}

module Samplizer.Config.Parse
  ( diceSpec
  ) where

-- Parse DnD dice roll string into a valid Options Partial
-- TODO: Ranges should be lists, not tuples with start/end (to accomodate discrete discontinuous values)
-- TODO: Maybe fail silently and just set values to mempty
-- TODO: Accept a maybe string?
-- TODO: Maybe have 40d{2,5} or 40d2..5 mean roll 40 times dice with sides numbered 2 thru 5?
-- TODO: General "number parser" which parses {2,5} and maybe even 2+-5 or {2+-5, 10}
-- TODO: Custom error type?
-- TODO: See https://mrkkrp.github.io/megaparsec/tutorials/parsing-simple-imperative-language.html for parsing things in parentheses (for grouping)
import Data.Monoid
import Text.Megaparsec
import Text.Megaparsec.Lexer (integer, signed)

import Samplizer.Config

type Parser = Parsec Dec String

diceSpec :: Parser (Options Partial)
diceSpec = do
  repeats' <- optional pRepeats
  rolls'   <- optional integer
  range' <- optional $ pDice
  targets' <- optional pTarget
  return $ Options (Last $ fst . singleValToRange <$> range')
                   (Last $ snd <$> range')
                   (Last $ fromIntegral <$> rolls')
                   (Last repeats')
                   (Last targets')

pRepeats :: Parser Int
pRepeats = try $ do
  rs <- integer
  _ <- char 'x'
  return $ fromIntegral rs

pTarget :: Parser (Int, Int)
pTarget = do
  space
  _ <- char '='
  space
  pRange minBound maxBound

-- | Parse the dice string
pDice :: Parser (Int, Int)
pDice = do
  _ <- char 'd'
  pRange' 1 (+5)

-- | Create a parser for a range with a default lower and upper bound value
pRange :: Int -> Int -> Parser (Int, Int)
pRange lower upper = pRange' lower (const upper)

-- | Create a parser with a lower bound and an upper bound-generating function
-- which takes the lower bound (first priority is what's specified in the
-- range itself, then the lower bound passed into this function)
-- lower and lowerMod are not absolute limits to the range, but rather default
-- values if a value is not specified
pRange' :: Int -> (Int -> Int) -> Parser (Int, Int)
pRange' lower lowerMod = try (pRange1 lowerMod) <|> pRange2 lower lowerMod

pRange1 :: (Int -> Int) -> Parser (Int, Int)
pRange1 lowerMod = do
  n1 <- signed space integer
  n2 <- option n1 $ do
    _ <- char '.'
    _ <- char '.'
    option (toInteger (lowerMod $ fromIntegral n1)) $ signed space integer

  return (fromIntegral n1, fromIntegral n2)

pRange2 :: Int -> (Int -> Int) -> Parser (Int, Int)
pRange2 lower lowerMod = do
  _ <- char '.'
  _ <- char '.'
  n2 <- option (toInteger (lowerMod lower)) $ signed space integer

  return (lower, fromIntegral n2)

singleValToRange :: (Int, Int) -> (Int, Int)
singleValToRange (floor', ceiling')
  | floor' == ceiling' = (1, ceiling')
  | otherwise          = (floor', ceiling')