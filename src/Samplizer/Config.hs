{-# LANGUAGE RecordWildCards, FlexibleInstances, StandaloneDeriving, PolyKinds, DataKinds, TypeOperators, TypeFamilies #-}

module Samplizer.Config where

import Data.Monoid

-- TODO: Reorganize modules? (move this to Samplizer.Config.Base)

data Phase = Partial | Complete

type family (p :: Phase) ::: a :: *
type instance Partial  ::: a = Last a
type instance Complete ::: a = a

data Options (p :: Phase) = Options
  { spaceFloor   :: p ::: Int
  , spaceCeiling :: p ::: Int
  , rolls        :: p ::: Int
  , repeats      :: p ::: Int
  , targets      :: p ::: (Int, Int)
  }

deriving instance Show (Options Partial)
deriving instance Show (Options Complete)

instance Monoid (Options Partial) where
  mempty = Options mempty mempty mempty mempty mempty
  mappend x y = Options
    { spaceFloor   = spaceFloor x   <> spaceFloor y
    , spaceCeiling = spaceCeiling x <> spaceCeiling y
    , rolls        = rolls x        <> rolls y
    , repeats      = repeats x      <> repeats y
    , targets      = targets x      <> targets y
    }

lastToEither :: Last a -> String -> Either String a
lastToEither (Last x) errMsg = maybe (Left errMsg) Right x

freeze :: Options Partial -> Either String (Options Complete)
freeze po = do
  spaceFloor   <- lastToEither (spaceFloor po) "missing: spaceFloor"
  spaceCeiling <- lastToEither (spaceCeiling po) "missing: spaceCeiling"
  rolls        <- lastToEither (rolls po) "missing: rolls"
  repeats      <- lastToEither (repeats po) "missing: repeats"
  targets      <- lastToEither (targets po) "missing: targets"
  return (Options{..} :: Options Complete)

freeze' :: Options Partial -> (String -> a) -> (Options Complete -> a) -> a
freeze' op b g = either b g $ freeze op