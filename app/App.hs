{-# LANGUAGE DataKinds #-}

module Main where

import Control.Monad        ((>=>))
import Data.Default
import Data.Monoid          (Last(..), (<>))
import Options.Applicative
import Text.Megaparsec

import Samplizer.Config
import Samplizer.Config.Default
import Samplizer.Config.Parse
import Samplizer.Sim

newtype SApp = SApp
  { argDiceSpec :: Maybe String
  }

lastOption :: Parser a -> Parser (Last a)
lastOption parser = Last <$> optional parser

freeze'' :: Options Partial -> (Options Complete -> IO ()) -> IO ()
freeze'' op = freeze' op (const $ print "Some internal error")

run :: SApp -> IO ()
run (SApp s) = either (putStrLn . parseErrorPretty) (\x -> freeze'' (unifyOps x) (execSims >=> print)) $ parse diceSpec "" s'
  where
    s' = maybe "" id s
    
    unifyOps :: Options Partial -> Options Partial
    unifyOps cmd = def <> cmd

main :: IO ()
main = execParser opts >>= run
  where
    opts = info (sapp <**> helper)
      ( fullDesc
     <> progDesc "Generate simulations of a certain number of random naturals generated"
     <> header "samplizer - random number simulator"
      )

sapp :: Parser SApp
sapp = SApp
  <$> optional (argument str
      ( metavar "DICE_SPEC"
     <> help "Specification for the simulation in quasi-DnD dice notation"
      ))
